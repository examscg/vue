import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Question1 from '@/pages/Question1'
import Question2 from '@/pages/Question2'
import Question3 from '@/pages/Question3'
import Question4 from '@/pages/Question4'
import CV from '@/pages/Cv'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/doscg/question1',
      name: 'Question1',
      component: Question1
    },
    {
      path: '/doscg/question2',
      name: 'Question2',
      component: Question2
    },
    {
      path: '/doscg/question3',
      name: 'Question3',
      component: Question3
    },
    {
      path: '/doscg/question4',
      name: 'Question4',
      component: Question4
    },
    {
      path: '/doscg/cv',
      name: 'CV',
      component: CV
    }
  ]
})
